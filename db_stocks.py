from influxdb import InfluxDBClient
from abc import abstractmethod
import sqlite3
from Learning.Dataset.timetools import superdate

class DataBase:
    def __init__(self):
        self.client = None
    @abstractmethod
    def insert_candles(self,  ticker: str, candles: list):
        pass

    @abstractmethod
    def insert_candle(self,  ticker: str, candles: list):
        pass

class SQLiteBase(DataBase):
    def __init__(self, db_path):
        super().__init__()
        self.client = sqlite3.connect(db_path)

    def insert_candles(self, ticker: str, candles: list):
        cursor = self.client.cursor()
        for candle in candles:
            sql = "INSERT INTO Hist VALUES('{}','{}',{},{},{},{},{});".format(
                ticker, str(candle['time']).split(' ')[0], candle['open'], candle['close'], candle['high'],
                candle['low'], candle['volume'])
            res = cursor.execute(sql)
        self.client.commit()
        return res

    def get_tickers(self, interval = 'day')->list:
        cursor = self.client.cursor()
        cursor.execute("SELECT DISTINCT ticker FROM Hist")
        ans = cursor.fetchall()
        return [c[0] for c in ans]

    def get_all_market_data(self, date_start, date_end):
        date_start = superdate(date_start)
        date_end = superdate(date_end)
        assert (date_end > date_start), "date_end must be greater than date_start"

        cursor = self.client.cursor()
        cursor.execute(
            """
            SELECT ticker, time, open, high, low, close, volume
            FROM Hist
            WHERE time BETWEEN ? and ?
            ORDER BY ticker ASC, time DESC
            """, [str(date_start), str(date_end)])

        ans = cursor.fetchall()
        self.close()
        return ans

    def close(self):
        self.client.close()

class InfluxBase:
    def __init__(self, host = 'localhost', port = '8086',
                       user = 'kirilman', password = '606613', dbname = 'tinkoff'):
        try:
            self.client = InfluxDBClient(host, port, user, password, dbname)
            self.client.switch_database(dbname)
        except Exception as err:
            print(err)

    def insert_candle(self, ticker: str, candle: dict):
        body = {
            "tags": {
                "ticker": ticker,
                "interval": candle['interval'],
            },
            "time": candle['time'],
            "measurement": "stocks",
            "fields": {
                "open":  candle['open'],
                "close": candle['close'],
                "high":  candle['high'],
                "low":   candle['low'],
                "volume": candle['volume'],
            }
        }

        res = self.client.write_points([body])
        return res

    def insert_candles(self, ticker: str, candles: list):
        json_body = []
        for candle in candles:
            body = {
                "tags": {
                    "ticker": ticker,
                    "interval": candle["interval"],
                },
                "time": candle["time"],
                "measurement":"stocks",
                "fields": {
                    "open" : candle["open"],
                    "close": candle["close"],
                    "high" : candle["high"],
                    "low"  : candle["low"],
                    "volume": candle["volume"],
                }
            }
            json_body.append(body)
        res = self.client.write_points(json_body, batch_size=10000)
        return res

    def get_candle_from_to(self, ticker, interval, _from, to):
        """
        :param ticker:
        :param interval:
        :param _from: str "2015-08-18T00:00:00Z"
        :param to: str "2015-08-18T00:00:00Z"
        :return candles: list
        """
        ans = self.client.query("SELECT * FROM stocks WHERE ticker = '{}' AND time > '{}' AND time < '{}'".
                                      format(ticker, _from, to))
        candles = ans.get_points(tags={'interval': interval})
        return list(candles)

    def get_list_tables(self):
        return self.client.get_list_database()

    def get_last_candle(self, ticker):
        ans = self.client.query("SELECT * FROM stocks WHERE ticker = '{}' ORDER BY time DESC LIMIT 1".format(ticker))
        return list(ans.get_points())

    def get_tickers(self, interval:str)->list:
        res = self.client.get_list_series(tags = {'interval': interval})
        return [ x.split('=')[2] for x in res]

if __name__ == '__main__':
    dbase = SQLiteBase('/home/office/kirilman/Datasets/SPB/tinkoff')
    tickers = dbase.get_tickers()
    print(tickers)
    print(len(tickers))

    hist = dbase.get_all_market_data('2005-01-01','2021-02-01')
    print(len(hist))