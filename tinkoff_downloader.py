from openapi_client import  openapi
from datetime import datetime, timedelta
from time import sleep
from abc import ABC, abstractmethod


class Downloader(ABC):
    def __init__(self):
        self.token

    @abstractmethod
    def get_candles_from_to(self, interval):
        return


class TinkoffDownloader(Downloader):
    def __init__(self):
        self.token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
        self.client = openapi.sandbox_api_client(self.token)

    def get_candles_from_to(self, figi = "BBG000BNSZP1",
                            start_day  = "01.12.2020",
                            end_day    = "12.12.2020",
                            interval   = "day"):
        """
        :param figi:
        :param start_day: "12.02.2020"
        :param end_day:
        :param interval:
        :return:
        """
        start = datetime.strptime(start_day, "%d.%m.%Y")
        end   = datetime.strptime(end_day, "%d.%m.%Y")
        differ  = end - start
        msk   = "+00:00"
        day_interval = timedelta(seconds=60 * 60 * 24 - 1)
        candles_list = []
        for day_year in range(differ.days):
            current_day = start + timedelta(days=day_year)
            current_start_string = current_day.isoformat() + msk
            current_end_string = (current_day + day_interval).isoformat() + msk

            response = self.client.market.market_candles_get(figi,
                                                             current_start_string,
                                                             current_end_string,
                                                             interval,
                                                             _request_timeout=15)

            for c in response.payload.candles:
                candles_list.append(c.to_dict())
            sleep(0.085)
        return candles_list

    def download_day_interval(self, figi, start_day, end_day):
        start = datetime.strptime(start_day, "%d.%m.%Y")
        end = datetime.strptime(end_day, "%d.%m.%Y")
        msk   = "+00:00"
        candles_list = []

        for y in range(end.year - start.year):
            s = datetime(start.year + y, 1, 1)
            e = datetime(start.year + y, 12, 31)
            start_string = s.isoformat() + msk
            end_string   = (e + timedelta(seconds=60 * 60 * 24 - 1)).isoformat() + msk
            response = self.client.market.market_candles_get(figi,
                                                             start_string,
                                                             end_string,
                                                             interval='day')

            for c in response.payload.candles:
                candles_list.append(c.to_dict())

        return candles_list

