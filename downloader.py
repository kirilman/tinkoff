from openapi_client import openapi
from tinkoff_downloader import TinkoffDownloader
import csv

def write_candles_to_cvs(file, candles):
    writer = csv.writer(file, delimiter=';')
    writer.writerow(('time', 'o', 'c', 'h', 'l', 'v'))

    for c in candles:
        d = {'time': c['time'], 'o': c['o'], 'c': c['c'], 'h': c['h'], 'l': c['l'], 'v': c['v']}
        writer.writerow(d.values())

def download_min_interval(start_day, end_day):
    token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
    client = openapi.sandbox_api_client(token)
    stocks = client.market.market_stocks_get()
    instuments = stocks.payload.instruments
    downloader = TinkoffDownloader()

    instuments = instuments[15:20]
    print('Количество инструментов ', len(instuments))
    while len(instuments) > 0:
        inst = instuments.pop()
        figi, ticker = inst.figi, inst.ticker
        try:
            candles = downloader.get_candles_from_to(figi=figi, start_day=start_day, end_day=end_day, interval="1min")
        except Exception as error:
            print('Failed to ', ticker, error)
            instuments.insert(0, inst)
            continue
        file = open('../../../Datasets/SPB/min' + ticker + '_1min.csv', 'w', newline='\n')
        write_candles_to_cvs(file, candles)
        file.close()
        print(ticker, ':', len(candles))

def download_day_interval(start_day, end_day):
    token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
    client = openapi.sandbox_api_client(token)
    stocks = client.market.market_stocks_get()
    instuments = stocks.payload.instruments
    downloader = TinkoffDownloader()
    instuments = instuments[100:110]
    print('Количество инструментов ', len(instuments))

    while len(instuments) > 0:
        inst = instuments.pop()
        figi, ticker = inst.figi, inst.ticker
        try:
            candles = downloader.download_day_interval(figi=figi, start_day=start_day, end_day=end_day)
        except Exception as error:
            print('Failed to ', ticker, error)
            instuments.insert(0, inst)
            continue
        file = open('../../../Datasets/SPB/day'+ticker+'.csv', 'w', newline='\n')
        write_candles_to_cvs(file, candles)
        file.close()
        print(ticker, ':', len(candles))

if __name__ == '__main__':

    # download_day_interval('01.01.2015', '01.01.2021')
    download_min_interval('01.01.2020','01.01.2021')
