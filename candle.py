
class Candle(dict):
    def __init__(self, open=0, close=0, high=0, low=0, volume=0, time=''):
        self.open = open
        self.close = close
        self.high = high
        self.low = low
        self.volume = volume
        self.time = time
        self.__dict__['open'] = self.open

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError as e:
            raise AttributeError(e)

    def __str__(self):
        return "{}: o={}, c={}, l={}, h={}, v={}".format(self.time, self.open, self.close, self.low, self.high, self.volume)

