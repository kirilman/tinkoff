import datetime as dt

_ds = dt.date(1970, 1, 1)
_de = dt.date(2030, 12, 31)

__str_to_date = {dt.datetime.strftime(d, '%Y-%m-%d'): d for d in
                 [_ds + dt.timedelta(days=i) for i in range((_de - _ds).days)]}
__date_to_str = {d: dt.datetime.strftime(d, '%Y-%m-%d') for d in
                 [_ds + dt.timedelta(days=i) for i in range((_de - _ds).days)]}


def _date_to_str(date):
    if isinstance(date, dt.date):
        return __date_to_str[date]
    if isinstance(date, dt.datetime):
        return __date_to_str[date.date()]
    raise ValueError('date must be of the datetime.datetime or datetime.date type')


def _str_to_date(datestring):
    if not isinstance(datestring, str):
        raise ValueError('datestring must be of python string type')
    return __str_to_date[datestring]


class superdate:
    """
    YYYY-MM-DD
    """

    def __init__(self, date):
        if isinstance(date, str):
            self._date = _str_to_date(date)
            return
        if isinstance(date, dt.datetime):
            self._date = date.date()
            return
        if isinstance(date, dt.date):
            self._date = date
            return
        if isinstance(date, superdate):
            self._date = date._date
            return
        raise ValueError("date must be of the superdate, datetime.datetime, date or str type")

    def __iadd__(self, v):
        self._date = self._date + dt.timedelta(days=int(v))
        return self

    def __isub__(self, v):
        self._date = self._date - dt.timedelta(days=int(v))
        return self

    def __add__(self, v):
        return superdate(self._date + dt.timedelta(days=int(v)))

    def __radd__(self, v):
        return self + v

    def __sub__(self, v):
        if isinstance(v, int):
            return superdate(self._date - dt.timedelta(days=int(v)))
        return (self._date - superdate(v)._date).days

    def __rsub__(self, v):
        return superdate(v) - self

    def __str__(self):
        return _date_to_str(self._date)

    def __repr__(self):
        return str(self)

    def __lt__(self, date):
        return self._date < superdate(date)._date

    def __le__(self, date):
        return self._date <= superdate(date)._date

    def __eq__(self, date):
        return self._date == superdate(date)._date

    def __ne__(self, date):
        return self._date != superdate(date)._date

    def __gt__(self, date):
        return self._date > superdate(date)._date

    def __ge__(self, date):
        return self._date >= superdate(date)._date

    def __hash__(self):
        return hash(str(self))

    def date_py(self):
        return self._date

    def copy(self):
        return superdate(self)


def _str_to_time(t):
    t = t.split(":")
    assert (2 <= len(t) <= 3), "time does not match the format (HH:MM:SS or HH:MM)"
    h = int(t[0])
    m = int(t[1])
    s = 0
    if len(t) == 3:
        s = int(t[2])
    assert (0 <= h < 24 and 0 <= m < 60 and 0 <= s < 60), "time does not match the format (HH:MM:SS or HH:MM)"
    return dt.time(h, m, s)


def _time_to_str(time):
    if not isinstance(time, dt.time):
        raise ValueError('only datetime.time type')
    return time.strftime("%H:%M:%S")


class supertime:
    def __init__(self, time):
        if isinstance(time, str):
            self._time = _str_to_time(time)
            return
        if isinstance(time, dt.time):
            self._time = time
            return
        if isinstance(time, supertime):
            self._time = time._time
            return
        if type(time) == float:
            time = int(time)
        if isinstance(time, int):
            time = time % 86400
            self._time = dt.time(hour=time // 3600, minute=time % 3600 // 60, second=time % 60)
            return
        raise ValueError("time must be of the supertime, datetime.time or python string type")

    def copy(self):
        return supertime(self)

    def totalseconds(self):
        return 3600 * self._time.hour + 60 * self._time.minute + self._time.second

    def __iadd__(self, v):
        self._time = supertime(self.totalseconds() + supertime(v).totalseconds())._time
        return self

    def __isub__(self, v):
        self += -supertime(v).totalseconds()
        return self

    def __add__(self, v):
        a = self.copy()
        a += v
        return a

    def __radd__(self, v):
        return self + v

    def __sub__(self, v):
        a = self.copy()
        a -= v
        return a

    def __rsub__(self, v):
        return supertime(v) - self

    def __str__(self):
        return _time_to_str(self._time)

    def __repr__(self):
        return str(self)

    def __lt__(self, time):
        return self._time < supertime(time)._time

    def __le__(self, time):
        return self._time <= supertime(time)._time

    def __eq__(self, time):
        return self._time == supertime(time)._time

    def __ne__(self, time):
        return self._time != supertime(time)._time

    def __gt__(self, time):
        return self._time > supertime(time)._time

    def __ge__(self, time):
        return self._time >= supertime(time)._time

    def __hash__(self):
        return hash(str(self))

    def time_py(self):
        return self._time
