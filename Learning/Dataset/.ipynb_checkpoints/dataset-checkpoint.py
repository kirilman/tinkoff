import numpy as np
from .timetools import superdate

class Dataset():
    def __init__(self, db_connect):
        pass
    def get_random(self):
        pass

class Hist():
    def __init__(self, first_date, last_date, dbase):
        """
            [d][m][open, high, low, close, volume]
        """
        self._fd = superdate(first_date)  # first date
        self._ld = superdate(last_date)  # last date

        self.db = dbase

        labels = self.db.get_tickers()
        ans = self.db.get_all_market_data(self._fd, self._ld)
        del self.db

        count_days = self._ld - self._fd + 1
        self.hist = np.zeros((count_days, len(labels), 5))
        self.hist.fill(np.NaN)
        self.days_idx = {(self._fd + i): i for i in range(count_days)}
        self.mark_idx = {label: i for i, label in enumerate(labels)}
        del labels
        for a in ans:
            d = self.days_idx[a[1]]
            m = self.mark_idx[a[0]]
            self.hist[d, m, :] = np.array(a[2:])
        del ans

    def get_period(self, date, days_range, labels=[]):
        """
            Получить выборку(срез) свечей с date по диапазону (days_range) вперед или назад.
            return: {'AAL' : {'2020-01-02': array([2.887300e+01, 2.918700e+01, 2.854500e+01, 2.898300e+01, 6.298825e+06]),
                              '2020-01-03': array([2.816600e+01, 2.818600e+01, 2.723900e+01, 2.754800e+01, 1.407187e+07])},
                     'SAVE': {'2020-01-02': array([4.070000e+01, 4.093000e+01, 4.010000e+01, 4.065000e+01, 1.027611e+06]),
                              '2020-01-03': array([3.966000e+01, 3.993000e+01, 3.833000e+01, 3.982000e+01, 1.327767e+06])}}
        """
        date = superdate(date)
        days = [date + i for i in days_range]
        ans = dict()

        if len(labels) == 0:
            labels = self.mark_idx

        for m in labels:
            if m in self.mark_idx:
                mi = self.mark_idx[m]
            else:
                continue
            if len(labels) != 0 and m not in labels:
                continue
            mark = dict()
            for d in days:
                if d in self.days_idx:
                    di = self.days_idx[d]
                else:
                    continue
                v = self.hist[di, mi, :]
                if np.isnan(v).sum() > 0:
                    continue
                mark[d] = v
            if len(mark) > 0:
                ans[m] = mark
        return ans

    def get_bar(self, date, ticker):
        date = superdate(date)
        assert (date in self.days_idx and ticker in self.mark_idx), ""
        d = self.days_idx[date]
        m = self.mark_idx[ticker]
        return self.hist[d, m, :]

    def preload(self, days_range):
        """
            return:
            dict = {'2015-01-10': {'AMD':{2015-01-10: array([2.887300e+01, 2.918700e+01, 2.854500e+01, 2.898300e+01, 6.298825e+06],
                                         {2015-01-09: arr...,
                                         {2015-01-08: arr...,
                                  'SAVE':{2015-01-10: arr...,
                                  ...
                    {'2015-01-11: {'AMD':{2015-01-10: arr..,

        """
        market_dict = dict()
        date = superdate(self._fd)
        while date <= self._ld:
            market_dict[date.copy()] = self.get_period(date, days_range)
            print('Preloading: ' + str(date) + '/' + str(self._ld), end='\r', flush=True)
            date += 1
        print()
        return market_dict

if __name__ == '__main__':
    pass

