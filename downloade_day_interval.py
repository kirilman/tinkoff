import sys
sys.path.append('Projects/Stock/Tinkoff/')
from openapi_client import openapi
from tinkoff_downloader import TinkoffDownloader
import os
import csv
from datetime import datetime

def write_candles_to_cvs(file, candles):
    writer = csv.writer(file, delimiter=';')
    writer.writerow(('time', 'o', 'c', 'h', 'l', 'v'))
    for c in candles:
        d = {'time': c['time'], 'o': c['o'], 'c': c['c'], 'h': c['h'], 'l': c['l'], 'v': c['v']}
        writer.writerow(d.values())

def downloade(start_day = '01.01.2005', end_day = '07.01.2021', interval = 'day', file_tickers = '700_spb.txt'):
    if interval == 'day':
        out_dir = '../../../Datasets/SPB/day/'
    elif interval == '1min':
        out_dir = '../../../Datasets/SPB/min/'
    downloaded = [f.split('.')[0] for f in os.listdir(out_dir)]
    downloader = TinkoffDownloader()

    token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
    client = openapi.sandbox_api_client(token)

    f = open(file_tickers, 'r')
    data = f.readlines()
    f.close()
    list_tickers = [d.replace('\n', '') for d in data]
    print('Количество котировок для загрузки {}'.format(len(list_tickers)))
    while len(list_tickers) > 0:
        ticker = list_tickers.pop()
        if ticker in downloaded:
            continue
        ans = client.market.market_search_by_ticker_get(ticker)
        if len(ans.payload.instruments) > 0:
            figi = ans.payload.instruments[0].figi
        else:
            continue
        try:
            if interval == 'day':
                candles = downloader.download_day_interval(figi, start_day, end_day)
            elif interval == '1min':
                candles = downloader.get_candles_from_to(figi, start_day, end_day, interval=interval)
        except Exception as err:
            print(err)
            continue
        file = open(out_dir + ticker + '.csv', 'w')
        write_candles_to_cvs(file, candles)
        file.close()
        cur_time = datetime.now()
        print(cur_time, ticker, len(candles))

if __name__ == '__main__':
    downloade('01.01.2005', '10.01.2021', interval='1min',file_tickers='700_spb.txt')
