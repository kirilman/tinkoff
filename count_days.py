from openapi_client import  openapi
from datetime import datetime
import pandas as pd
import os
from Tinkoff.db_stocks import InfluxBase
token = "t.wlnxazSuK6W--U6od2e6k778AW3nfY7MPm-1h93rzYXl6JIVPTnODfaeaJz1EjhfWl6XnJn2MCmIlqJPeeF6tg"
client = openapi.sandbox_api_client(token)
def get_history():
    r = client.market.market_search_by_ticker_get('AMD')
    figi = r.payload.instruments[0].figi
    msk = "+00:00"
    start = datetime.strptime('01.01.2003', "%d.%m.%Y").isoformat() + "+00:00"
    end = datetime.strptime('08.01.2003', "%d.%m.%Y").isoformat() + "+00:00"
    h = client.market.market_candles_get(figi, start, end, 'day')
    print(h)

def write_count_trade_days(start_date = datetime(2010,1,1), end_date = datetime(2021,1,1)):
    dbase = InfluxBase()
    file = open('Tinkoff/resource/count_days.txt','w')
    tickers = dbase.get_tickers('day')

    for ticker in tickers:
        res = dbase.get_candle_from_to(ticker,'day',start_date.isoformat().split('T')[0], end_date.isoformat().split('T')[0])
        frame = pd.DataFrame(res)
        frame['time'] = frame.time.apply(lambda x:pd.to_datetime(x.split('T')[0]))
        frame.set_index('time',inplace=True)

        count_all_days = 0
        count_trade_days = 0
        for date in pd.date_range(start_date, end_date):
            count_all_days+=1
            if date in frame.index:
                count_trade_days+=1
        file.write('{};{};{}\n'.format(ticker, round(count_trade_days/count_all_days,3), count_trade_days))
    file.close()
# print(os.listdir())
write_count_trade_days()