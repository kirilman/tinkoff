from datetime import datetime
import holidays
from workalendar.europe import Russia

def is_day_holiday(date:datetime):
    us_holidays = holidays.UnitedStates()
    return date in us_holidays

def is_day_off(date:datetime):
    """
        День выходной?
    """
    return not Russia().is_working_day(date)
