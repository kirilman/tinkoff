import os
import csv
from datetime import datetime

"""
    candle 
        _from_dict
        _to_dict
        _from_list
        _serialize
"""


class Stock_tracker:
    def __init__(self, downloader, dbase):
        self.dbase = dbase
        self.downloader = downloader

    def insert_csvfiles_to_base(self, directory, interval):
        assert (interval in ['1min', 'day']), "interval is bad"
        files = os.listdir(directory)
        for file in files:
            f = open(os.path.join(directory, file), 'r')
            ticker = file.split('.')[0]
            reader = csv.reader(f, delimiter=';', )
            columns = next(reader)
            assert (columns == ['time', 'o', 'c', 'h', 'l', 'v']), "Названия столбцов несоответсвуют"
            candles = []
            for row in reader:
                candles.append({'open': float(row[1]),
                                'close': float(row[2]),
                                'high': float(row[3]),
                                'low': float(row[4]),
                                'volume': float(row[5]),
                                'time': datetime.fromisoformat(row[0]),
                                'interval': interval})

            self.dbase.insert_candles(ticker, candles)
            f.close()
            print(ticker, len(candles))
